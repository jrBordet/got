//
//  articleStoreTests.m
//  got
//
//  Created by Jean Raphael Bordet on 21/10/15.
//  Copyright © 2015 Jean Raphael Bordet. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Article.h"
#import "ArticleStore.h"

@interface articleStoreTests : XCTestCase

@end

@implementation articleStoreTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testCreateArticle {
    Article *article = [[ArticleStore sharedStore]createArticle];
    
    XCTAssertEqual(article.title, @"Season 6");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
