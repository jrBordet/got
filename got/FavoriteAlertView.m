//
//  FavoriteAlertView.m
//  got
//
//  Created by Jean Raphael Bordet on 21/10/15.
//  Copyright © 2015 Jean Raphael Bordet. All rights reserved.
//

#import "FavoriteAlertView.h"
#import "ModelMasterView.h"

@implementation FavoriteAlertView

static NSString *removeFavorite = @"Remove from favorites?";
static NSString *addFavorite = @"Add to favorites?";

#pragma mark - AlertView 

- (id)init {
    self = [super init];
    
    if(self) {
        Article *article = [[[ModelMasterView sharedModel] articlesFiltered] objectAtIndex:self.indexPath.row];
            
        NSString *title = article.favorite ? removeFavorite : addFavorite;
            
        self = [super initWithTitle:title message: article.title delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    }
    
    return self;
}

- (id)initFavoritesAtIndex: (NSIndexPath *)indexPath {
    self.indexPath = indexPath;

    return [self init];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSArray *articles = [[ModelMasterView sharedModel] articlesFiltered];
    
   if (buttonIndex == 1) {
        if ([alertView.title isEqualToString:removeFavorite]) {
            [articles[self.indexPath.row] setFavorite:NO];
        } else {
            [articles[self.indexPath.row] setFavorite:YES];
        }
    }
    
    [self.favoriteDelegate favoritesChanged];
}

@end
