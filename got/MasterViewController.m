//
//  MasterViewController.m
//  got
//
//  Created by Jean Raphael Bordet on 20/10/15.
//  Copyright © 2015 Jean Raphael Bordet. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "ModelMasterView.h"
#import "FavoriteAlertView.h"
#import "ArticleCell.h"
#import "UIColor+FavoritesColor.h"

@interface MasterViewController ()

@end

@implementation MasterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Bookmar button
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemBookmarks target:self action:@selector(filterFavorites)];
    addButton.tintColor = [UIColor favoriteOrangeColor];
    self.navigationItem.rightBarButtonItem = addButton;
    self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
    
    // Refresh button
    UIBarButtonItem *refreshButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refresh)];
    self.navigationItem.leftBarButtonItem = refreshButton;
    
    // fetch Articles
    [self fetchArticles];
    
    // attach long press gesture to tableView
    [self addLongPress];
    
    // Model Delegate
    [[ModelMasterView sharedModel] setDelegate:self];
    
    // Activity Indicator
    [self.downloadActivityIndicator setHidesWhenStopped:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    self.clearsSelectionOnViewWillAppear = self.splitViewController.isCollapsed;
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Articles

- (void) refresh {
     [[ModelMasterView sharedModel] fetchArticles];
}

- (void)filterFavorites {
    [[ModelMasterView sharedModel] filterFavorites];
    
    [[self tableView] reloadData];
}

- (void) fetchArticles {
    [self.downloadActivityIndicator setHidden:false];
    [self.downloadActivityIndicator startAnimating];
    
    [[ModelMasterView sharedModel] fetchArticles];
}

#pragma mark - Favorites Gesture Recognizer

- (void)addLongPress {
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    
    lpgr.minimumPressDuration = .2;
    lpgr.delegate = self;
    lpgr.delaysTouchesBegan = YES;
    
    [self.tableView addGestureRecognizer:lpgr];
}

- (void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer {
    if (![[ModelMasterView sharedModel] favoritesToShow]) {
        return;
    }
    
    CGPoint p = [gestureRecognizer locationInView:self.tableView];
    
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
    
    if (indexPath == nil) {
        NSLog(@"long press somewhere");
    } else if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        [self showAlertForFavoritesAtIndex:indexPath];
    }
}

#pragma mark - AlertView Favorites

- (void)showAlertForFavoritesAtIndex: (NSIndexPath *)indexPath {
    FavoriteAlertView *fav = [[FavoriteAlertView alloc]initFavoritesAtIndex:indexPath];
    [fav setFavoriteDelegate:self];
    [fav show];
}

- (void)favoritesChanged {
    [self.downloadActivityIndicator setHidden:true];
    [self.downloadActivityIndicator stopAnimating];
    
    [self.tableView reloadData];
}

#pragma mark - Segues

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ([[ModelMasterView sharedModel] favoritesToShow]) {
        return YES;
    } else {
        return NO;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
            
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Article *object = [[[ModelMasterView sharedModel] articlesFiltered] objectAtIndex:indexPath.row];
            
        DetailViewController *controller = (DetailViewController *)[[segue destinationViewController] topViewController];
        [controller setDetailArticle:object];
            
        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        controller.navigationItem.leftItemsSupplementBackButton = YES;
    }
}

#pragma mark - ModelMasterView Delegate

- (void)downloadCompleted {
    [self.downloadActivityIndicator setHidden:true];
    [self.downloadActivityIndicator startAnimating];

    [[self tableView] reloadData];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[ModelMasterView sharedModel] tableViewRows];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ArticleCell *cell = (ArticleCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    [[ModelMasterView sharedModel] tableViewArticleCell:cell AtIndex:indexPath];
        
    return cell;
}

@end
