//
//  UIColor+FavoritesColor.h
//  got
//
//  Created by Jean Raphael Bordet on 23/10/15.
//  Copyright © 2015 Jean Raphael Bordet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (FavoritesColor)

+(UIColor *) favoriteOrangeColor;

@end
