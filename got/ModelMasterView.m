//
//  ModelMasterView.m
//  got
//
//  Created by Jean Raphael Bordet on 21/10/15.
//  Copyright © 2015 Jean Raphael Bordet. All rights reserved.
//

#import "ModelMasterView.h"
#import "ArticleStore.h"
#import "UIColor+FavoritesColor.h"

@implementation ModelMasterView

static NSString *baseUrl = @"http://gameofthrones.wikia.com/api/v1/Articles/Top";

+ (instancetype)sharedModel {
    static ModelMasterView *sharedModel = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedModel = [[self alloc] initPrivate];
    });
    
    return sharedModel;
}

- (instancetype)init {
    @throw [NSException exceptionWithName:@"Singleton"
                                   reason:@"Use +[ModelMasterView sharedModel]"
                                 userInfo:nil];
    return nil;
}

- (instancetype)initPrivate {
    self = [super init];
    if (self) {
        self.favorites = false;
        [[HttpClient sharedClient] setDelegate:self];
    }
    return self;
}

- (void)filterFavorites {
    if (self.favorites) {
        self.favorites = NO;
    } else {
        self.favorites = YES;
    }
}

- (NSArray *)articlesFiltered {
    NSArray *articles = [[ArticleStore sharedStore] allArticles];
    
    if (self.favorites) {
        NSPredicate *inPredicate = [NSPredicate predicateWithFormat: @"favorite == YES", articles];
        articles = [articles filteredArrayUsingPredicate:inPredicate];
    }
    
    return articles;
}

- (NSInteger)tableViewRows {
    NSArray *articles = [self articlesFiltered];
    
    if (articles.count == 0) {
        return 1;
    } else {
        return articles.count;
    }
}

- (void)tableViewArticleCell:(ArticleCell *) cell AtIndex:(NSIndexPath *)indexPath {
    NSArray *articles = [self articlesFiltered];
    
    if (articles.count == 0) {
        self.favoritesToShow = NO;
        
        cell.title.text = @"No favorites";
        cell.title.textColor = [UIColor favoriteOrangeColor];
        cell.abstract.text = @"use long press to add your favorites";
        cell.thumbnail.image = nil;
    } else {
        self.favoritesToShow = YES;
        
        Article *article = articles[indexPath.row];
        
        cell.title.text = article.title;
        cell.abstract.text = article.abstract;
        
        if (article.favorite) {
            cell.title.textColor = [UIColor favoriteOrangeColor];
        } else {
            cell.title.textColor = [UIColor blackColor];
        }
        
        [self fetchThumbnailCell:cell WithUrl:article.thumbnail];
    }
}

- (void)fetchThumbnailCell: (ArticleCell *) cell WithUrl: (NSString *)sUrl {
    [[HttpClient sharedClient] fetchImageFromUrl:[NSURL URLWithString:sUrl] placheholderImage:cell.thumbnail];
}

#pragma mark - Articles http

- (void)fetchArticles {
    [[ArticleStore sharedStore] removeAllArticles];
    
    NSDictionary *queryString = @ {
        @"expand": @"1",
        @"Category": @"Characters",
        @"limit": @"75"
    };

    [[HttpClient sharedClient] performRequestWith:baseUrl query:queryString];
}

/**
 *  This method is used for parsing the json response from API. For each elements
 *  parsed a corresponding element in ArticleStore is created
 *
 *  @param jsonObject
 */
- (void)parseJSONObject:(NSDictionary *)jsonObject {
    for (id object in jsonObject[@"items"]) {
        Article * article = [[Article alloc]initWithTitle:object[@"title"]
                                             Basepath:jsonObject[@"basepath"]
                                                  Url:object[@"url"]
                                             Abstract:object[@"abstract"]
                                            Thumbnail:object[@"thumbnail"]];
    
        [[ArticleStore sharedStore] addArticle:article];
    }
}

#pragma mark - HttpClient Delegate

-(void)downloadCompletedWith: (NSDictionary *)data {
     [self parseJSONObject:data];
    
    if (self.delegate) {
        [self.delegate downloadCompleted];
    }
}

@end
