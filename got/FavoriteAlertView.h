//
//  FavoriteAlertView.h
//  got
//
//  Created by Jean Raphael Bordet on 21/10/15.
//  Copyright © 2015 Jean Raphael Bordet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Article.h"

@protocol FavoriteDelegate

@required
- (void)favoritesChanged;
@end

@interface FavoriteAlertView : UIAlertView <UIAlertViewDelegate>

@property (nonatomic, weak) id<FavoriteDelegate> favoriteDelegate;

@property (nonatomic, strong) NSIndexPath *indexPath;

- (id) initFavoritesAtIndex: (NSIndexPath *)indexPath;

@end
