//
//  main.m
//  got
//
//  Created by Jean Raphael Bordet on 20/10/15.
//  Copyright © 2015 Jean Raphael Bordet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
