//
//  DetailViewController.h
//  got
//
//  Created by Jean Raphael Bordet on 20/10/15.
//  Copyright © 2015 Jean Raphael Bordet. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Article;

@interface DetailViewController : UIViewController

@property (strong, nonatomic) Article *detailArticle;

@property (nonatomic, strong) UIImageView *thumbnailImage;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *abstractLabel;
@property (nonatomic, strong) UIButton *safariButton;

@end

