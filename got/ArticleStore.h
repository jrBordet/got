//
//  ArticleStore.h
//  got
//
//  Created by Jean Raphael Bordet on 21/10/15.
//  Copyright © 2015 Jean Raphael Bordet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Article.h"

@interface ArticleStore : NSObject {
    NSMutableArray *allItems;
}

+ (instancetype)sharedStore;

- (NSArray *)allArticles;
- (Article *)createArticle;
- (void)removeAllArticles;
- (void)addArticle: (Article *)article;

@end
