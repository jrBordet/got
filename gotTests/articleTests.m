//
//  gotTests.m
//  gotTests
//
//  Created by Jean Raphael Bordet on 20/10/15.
//  Copyright © 2015 Jean Raphael Bordet. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Article.h"

@interface gotTests : XCTestCase

@end

@implementation gotTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testArticleDefault {
    
    Article *article = [[Article alloc]init];
    
    XCTAssertEqual(article.title, @"Season 6");
    XCTAssertEqual(article.basepath, @"http://gameofthrones.wikia.com");
    XCTAssertEqual(article.url, @"/wiki/Season_6");
    XCTAssertEqual(article.abstract, @"Season 6 of Game of Thrones was formally commissioned by HBO on 8 April 2014, following a...");
    XCTAssertEqual(article.thumbnail, @"http://vignette2.wikia.nocookie.net/gameofthrones/images/e/e5/1508_promo_stills_12001692731.jpg/revision/latest/window-crop/width/200/x-offset/818/y-offset/0/window-width/2101/window-height/2100?cb=20150601040408");

}

- (void)testArticleInit {
    
    Article *article = [[Article alloc]initWithTitle:@"Season 6"
                                            Basepath:@"http://gameofthrones.wikia.com"
                                                 Url:@"/wiki/Season_6"
                                            Abstract:@"Season 6 of Game of Thrones was formally commissioned by HBO on 8 April 2014, following a..."
                                           Thumbnail:@"http://vignette2.wikia.nocookie.net/gameofthrones/images/e/e5/1508_promo_stills_12001692731.jpg/revision/latest/window-crop/width/200/x-offset/818/y-offset/0/window-width/2101/window-height/2100?cb=20150601040408"];

    XCTAssertEqual(article.title, @"Season 6");
    XCTAssertEqual(article.basepath, @"http://gameofthrones.wikia.com");
    XCTAssertEqual(article.url, @"/wiki/Season_6");
    XCTAssertEqual(article.abstract, @"Season 6 of Game of Thrones was formally commissioned by HBO on 8 April 2014, following a...");
    XCTAssertEqual(article.thumbnail, @"http://vignette2.wikia.nocookie.net/gameofthrones/images/e/e5/1508_promo_stills_12001692731.jpg/revision/latest/window-crop/width/200/x-offset/818/y-offset/0/window-width/2101/window-height/2100?cb=20150601040408");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
