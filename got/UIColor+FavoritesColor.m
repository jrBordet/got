//
//  UIColor+FavoritesColor.m
//  got
//
//  Created by Jean Raphael Bordet on 23/10/15.
//  Copyright © 2015 Jean Raphael Bordet. All rights reserved.
//

#import "UIColor+FavoritesColor.h"

@implementation UIColor (FavoritesColor)

+ (UIColor *)favoriteOrangeColor {
    
    static UIColor *favoriteOrangeColor;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        favoriteOrangeColor = [UIColor colorWithRed:255.0f/255.0f green:124.0f/255.0f blue:60.0f/255.0f alpha:1.0];
    });
    
    return favoriteOrangeColor;
}

@end
