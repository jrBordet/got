//
//  Article.h
//  got
//
//  Created by Jean Raphael Bordet on 21/10/15.
//  Copyright © 2015 Jean Raphael Bordet. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Article : NSObject

@property (nonatomic, strong) NSString *key;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *basepath;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *abstract;
@property (nonatomic, strong) NSString *thumbnail;
@property (nonatomic, assign) BOOL favorite;

- (id)initWithTitle: (NSString *)t Basepath: (NSString *)basePath Url: (NSString *)url Abstract: (NSString *)abstract Thumbnail: (NSString *)thumbnail;

- (NSURL *)articleUrl;

@end
