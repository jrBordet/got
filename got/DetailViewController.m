//
//  DetailViewController.m
//  got
//
//  Created by Jean Raphael Bordet on 20/10/15.
//  Copyright © 2015 Jean Raphael Bordet. All rights reserved.
//

#import "DetailViewController.h"
#import "Article.h"
#import "Masonry.h"
#import "UIColor+FavoritesColor.h"
#import "HttpClient.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

#pragma mark - Managing the detail item

- (void)setDetailArticle:(Article *)newArticle {
    if (_detailArticle != newArticle) {
        _detailArticle = newArticle;
        
        [self configureView];
    }
}

- (void)configureView {
    if (_detailArticle) {
        UIView *superview = self.view;
        
         // thumbnailImage
        self.thumbnailImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        [self.view addSubview:self.thumbnailImage];
        
        CGFloat multiplier = 0.5;
        [self.thumbnailImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(superview.mas_top).offset(self.navigationController.navigationBar.frame.size.height + 30);
            make.centerX.equalTo(superview);
            make.width.equalTo(superview.mas_width).multipliedBy(multiplier);
            make.height.equalTo(superview.mas_width).multipliedBy(multiplier);
        }];
        
        self.thumbnailImage.layer.cornerRadius = (superview.frame.size.width / 2) * multiplier;
        self.thumbnailImage.layer.masksToBounds = YES;
        self.thumbnailImage.layer.borderWidth = 0;
        
        [self fetchThumbnailImageWithUrl:self.detailArticle.thumbnail];
        
         // titleLabel
        self.titleLabel = [UILabel new];
        self.titleLabel.text = _detailArticle.title;
        self.titleLabel.font = [UIFont boldSystemFontOfSize:18.0f];
        if (self.detailArticle.favorite) {
            self.titleLabel.textColor = [UIColor favoriteOrangeColor];
        } else {
            self.titleLabel.textColor = [UIColor blackColor];
        }
        
        [self.view addSubview:self.titleLabel];
        
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(superview.mas_centerY).multipliedBy(1.2);
            make.centerX.equalTo(superview);
        }];
        
        // abstractLabel
        self.abstractLabel = [UILabel new];
        self.abstractLabel.text = self.detailArticle.abstract;
        self.abstractLabel.numberOfLines = 3;
        self.abstractLabel.textAlignment = NSTextAlignmentJustified;
        [self.view addSubview:self.abstractLabel];
        
        [self.abstractLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.titleLabel.mas_bottom).offset(20);
            make.centerX.equalTo(superview.mas_centerX);
            make.width.equalTo(superview.mas_width).multipliedBy(0.9);;
        }];
        
        // safariButton
        self.safariButton = [UIButton buttonWithType:UIButtonTypeSystem];
        [self.safariButton addTarget:self
                   action:@selector(openSafari)
         forControlEvents:UIControlEventTouchUpInside];
        
        if (self.detailArticle.favorite) {
            [self.safariButton setTitle:@"view your favorite article on Safari" forState:UIControlStateNormal];
        } else {
            [self.safariButton setTitle:@"view on Safari" forState:UIControlStateNormal];
        }
        
        [self.view addSubview:self.safariButton];
        
        [self.safariButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.abstractLabel.mas_bottom).offset(30);
            
            make.centerX.equalTo(superview);
        }];
    }
}

- (void)fetchThumbnailImageWithUrl: (NSString *)sUrl {
    [[HttpClient sharedClient] fetchImageFromUrl:[NSURL URLWithString:sUrl] placheholderImage:self.thumbnailImage];
}

- (void)openSafari {
    [[UIApplication sharedApplication] openURL:self.detailArticle.articleUrl];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
