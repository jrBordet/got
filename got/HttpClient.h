//
//  HttpClient.h
//  got
//
//  Created by Jean Raphael Bordet on 26/10/15.
//  Copyright © 2015 Jean Raphael Bordet. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;

@protocol HttpClientProtocol <NSObject>

@required
-(void)downloadCompletedWith: (NSDictionary *)data;

@end

@interface HttpClient : NSObject <NSURLSessionDataDelegate>

@property(nonatomic, weak) id<HttpClientProtocol> delegate;

@property (nonatomic) NSURLSession *session;
@property (nonatomic, copy) NSArray *courses;

+(instancetype)sharedClient;
-(void)performRequestWith: (NSString *)url query: (NSDictionary *)q;
-(void)fetchImageFromUrl: (NSURL *)url placheholderImage:(UIImageView *) placeholder;

@end
