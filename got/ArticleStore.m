//
//  ArticleStore.m
//  got
//
//  Created by Jean Raphael Bordet on 21/10/15.
//  Copyright © 2015 Jean Raphael Bordet. All rights reserved.
//

#import "ArticleStore.h"

@implementation ArticleStore

+ (id)sharedStore {
    static ArticleStore *sharedStore = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedStore = [[self alloc] initPrivate];
    });
    
    return sharedStore;
}

- (instancetype)initPrivate {
    if (self = [super init]) {
        allItems = [[NSMutableArray alloc] init];
    }
    return self;
}

- (instancetype)init {
    @throw [NSException exceptionWithName:@"Singleton"
                                   reason:@"Use +[ArticleStore sharedStore]"
                                 userInfo:nil];
    return nil;
}

- (NSArray *) allArticles {
    return allItems;
}

- (Article *)createArticle {
    Article *article = [[Article alloc]init];
    
    [allItems addObject:article];
    
    return article;
}

- (void) addArticle: (Article *)article {
    [allItems addObject:article];
}

- (void)removeAllArticles {
    [allItems removeAllObjects];
}

@end
