//
//  ArticleCell.h
//  got
//
//  Created by Jean Raphael Bordet on 22/10/15.
//  Copyright © 2015 Jean Raphael Bordet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArticleCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *thumbnail;
@property (strong, nonatomic)  UILabel *title;
@property (strong, nonatomic)  UILabel *abstract;

@end
