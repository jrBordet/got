//
//  ArticleCell.m
//  got
//
//  Created by Jean Raphael Bordet on 22/10/15.
//  Copyright © 2015 Jean Raphael Bordet. All rights reserved.
//

#import "ArticleCell.h"
#import "Masonry.h"

@implementation ArticleCell

- (void)awakeFromNib {    
    CGFloat paddingTop = 10;
    
    // thumbnail
    UIEdgeInsets thumbNailPadding = UIEdgeInsetsMake(15, 10, 10, 10);

    [self.thumbnail mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView).with.insets(thumbNailPadding);
        make.top.equalTo(self.contentView.mas_top).with.offset(thumbNailPadding.top);
    }];
    
    // embed the thumbanail in a circle
    NSLog(@"height: %@", self.thumbnail.mas_height);
    NSLog(@"height: %f width: %f", self.thumbnail.frame.size.height, self.thumbnail.frame.size.width);
    
    self.thumbnail.layer.cornerRadius = self.thumbnail.frame.size.height / 2;
    self.thumbnail.layer.masksToBounds = YES;
    self.thumbnail.layer.borderWidth = 0;
    
     // title
    UIEdgeInsets titlePadding = UIEdgeInsetsMake(-5, self.thumbnail.frame.size.width + 20, 30, 10);
    self.title = [UILabel new];
    [self.contentView addSubview:self.title];
  
    self.title.font = [UIFont boldSystemFontOfSize:18.0f];
    
    [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView).with.insets(titlePadding);
        make.height.equalTo(self.contentView.mas_width).multipliedBy(0.15);
    }];
    
     // abstract
    self.abstract = [UILabel new];
    [self.contentView addSubview:self.abstract];
    self.abstract.font = [UIFont boldSystemFontOfSize:14.0f];

    UIEdgeInsets abstractPadding = UIEdgeInsetsMake(paddingTop + self.title.frame.size.height + 20, self.thumbnail.frame.size.width + 20, 0, 10);
    
    self.abstract.font = [UIFont systemFontOfSize:14];
    
    [self.abstract mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView).with.insets(abstractPadding);
        make.width.equalTo(@100);
        make.height.equalTo(self.contentView.mas_width).multipliedBy(0.15);
    }];
    
    self.abstract.numberOfLines = 2;

    NSLog(@"%f", self.title.frame.size.height);
}

@end
