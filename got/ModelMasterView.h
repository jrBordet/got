//
//  ModelMasterView.h
//  got
//
//  Created by Jean Raphael Bordet on 21/10/15.
//  Copyright © 2015 Jean Raphael Bordet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Article.h"
#import "ArticleCell.h"
#import "HttpClient.h"
@import UIKit;

@protocol ModelMasterViewDelegate

@required
- (void)downloadCompleted;
@end

/**
 *  favoritesToShow is used to handle the favorite's list count == 0 and display a custom cell
 */
@interface ModelMasterView : NSObject <HttpClientProtocol>

@property (nonatomic, weak) id<ModelMasterViewDelegate> delegate;

@property (nonatomic) BOOL favorites;
@property (nonatomic) BOOL favoritesToShow;

- (void)filterFavorites;
+ (instancetype)sharedModel;
- (NSInteger)tableViewRows;
- (void) tableViewArticleCell:(ArticleCell *) cell AtIndex:(NSIndexPath *)indexPath;
- (void)fetchArticles;
- (NSArray *) articlesFiltered;

@end
