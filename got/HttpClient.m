//
//  HttpClient.m
//  got
//
//  Created by Jean Raphael Bordet on 26/10/15.
//  Copyright © 2015 Jean Raphael Bordet. All rights reserved.
//

#import "HttpClient.h"
#import "FTWCache.h"
#import "NSString+MD5.h"

@implementation HttpClient

+ (id)sharedClient {
    static HttpClient *sharedClient = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedClient = [[self alloc] initPrivate];
    });
    
    return sharedClient;
}

- (instancetype)init {
    @throw [NSException exceptionWithName:@"Singleton"
                                   reason:@"Use +[HttpClient sharedClient]"
                                 userInfo:nil];
    return nil;
}

- (instancetype)initPrivate {
    self = [super init];
    if (self) {
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        self.session = [NSURLSession sessionWithConfiguration:config
                                                     delegate:self
                                                delegateQueue:nil];
    }
    return self;
}

-(void)performRequestWith: (NSString *)url query: (NSDictionary *)q {
    NSURLRequest *req = [NSURLRequest requestWithURL:[self url:url WithQuery:q]];
    
    NSURLSessionDataTask *dataTask = [self.session dataTaskWithRequest:req
                                                     completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                         
                                                         NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:data
                                                                                                                    options:0
                                                                                                                      error:nil];
                                                         
                                                         dispatch_async(dispatch_get_main_queue(), ^{
                                                             if (self.delegate) {
                                                                 [self.delegate downloadCompletedWith:jsonObject];
                                                             }
                                                         });
                                                     }];
    [dataTask resume];
}

-(NSURL *)url: (NSString *)sUrl WithQuery: (NSDictionary *)q {
    NSMutableString *urlWithQuery = [NSMutableString stringWithFormat:@"%@?", sUrl];
    NSMutableArray *parts = [NSMutableArray array];
    
    for (NSString *params in q) {
        NSString *part = [NSString stringWithFormat: @"%@=%@", params, [q objectForKey:params]];
        [parts addObject: part];
    }
    
    [urlWithQuery appendString:[parts componentsJoinedByString:@"&"]];
     
    return [NSURL URLWithString:urlWithQuery];
}

-(void)fetchImageFromUrl: (NSURL *)url placheholderImage:(UIImageView *)placeholder {
    NSString *key = [url.absoluteString MD5Hash];
    NSData *data = [FTWCache objectForKey:key];
    
    if (data) {
        UIImage *image = [UIImage imageWithData:data];
        placeholder.image = image;
    } else {
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^{
            NSData *data = [NSData dataWithContentsOfURL:url];
        
            UIImage *imageFromData = [UIImage imageWithData:data];
        
            [FTWCache setObject:UIImagePNGRepresentation(imageFromData) forKey:key];
            UIImage *imageToSet = imageFromData;
        
            if (imageToSet) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            placeholder.image = imageFromData;
                        });
            }
        });
    }
}

@end
