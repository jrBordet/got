//
//  MasterViewController.h
//  got
//
//  Created by Jean Raphael Bordet on 20/10/15.
//  Copyright © 2015 Jean Raphael Bordet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModelMasterView.h"
#import "FavoriteAlertView.h"

@class DetailViewController;

@interface MasterViewController : UITableViewController <UIGestureRecognizerDelegate, ModelMasterViewDelegate, FavoriteDelegate>

@property (strong, nonatomic) DetailViewController *detailViewController;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *downloadActivityIndicator;

@end

