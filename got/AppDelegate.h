//
//  AppDelegate.h
//  got
//
//  Created by Jean Raphael Bordet on 20/10/15.
//  Copyright © 2015 Jean Raphael Bordet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

